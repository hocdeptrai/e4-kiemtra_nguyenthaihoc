﻿namespace KiemTra_NThaiHoc.Models
{
    public class Report
    {
        public int ReportId { get; set; }
        public int AccountId { get; set; }
        public Accounts? Accounts { get; set; }
        public int LogsId { get; set; }
        public int TransactionalId { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
