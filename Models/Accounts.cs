﻿namespace KiemTra_NThaiHoc.Models
{
    public class Accounts
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AccountName { get; set; }
        public Customer? Customer { get; set; }
        public List<Report> Reports { get; set; }
    }
}
